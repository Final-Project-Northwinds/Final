﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Northwind.Models;
using Northwind.Security;
using System.Web.Security;
using System.Net;
using System.Data.Entity.Validation;


namespace Northwind.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee/Account
        
        [CustomAuthorization(LoginPage = "~/Employee/SignIn")]
        public ActionResult Account()
        {
            
            using (var db = new carNorthwindsEntities())
            {
                // find customer using EmployeeID (stored in authentication ticket)
                Employee employee = db.Employees.Find(UserAccount.GetUserID());
                // display original values in textboxes when customer is editing data
                EmployeeEdit EditEmployee = new EmployeeEdit()
                {
                    
                    LastName = employee.LastName,
                    FirstName = employee.FirstName,
                    Title = employee.Title,
                    Address = employee.Address,
                    City = employee.City,
                    Region = employee.Region,
                    PostalCode = employee.PostalCode,
                    Country = employee.Country,
                    HomePhone = employee.HomePhone,
                    Extension = employee.Extension,
                    Email = employee.Email
                };
                return View(EditEmployee);
            }
        }
        // GET: Customer/Register
        public ActionResult Register()
        {
            return View();
        }
        // GET: Employee/SignIn
        public ActionResult SignIn()
        {
            using (carNorthwindsEntities db = new carNorthwindsEntities())
            {
                // create drop-down list box for company name
                ViewBag.EmployeeId = new SelectList(db.Employees.OrderBy(e=> e.Email), "EmployeeID", "Email").ToList();
            }
            return View();

        }
        // POST: Customer/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "Email,Password,FirstName,LastName,Title,TitleOfCourtesy,Address,City,Region,PostalCode,Country,HomePhone,Extension,BirthDate,HireDate")] Employee employee)
        {

            try
            {
                using (carNorthwindsEntities db = new carNorthwindsEntities())
                {
                    // first, make sure the CompanyName is unique
                    if (db.Employees.Any(e => e.Email == employee.Email))
                    {
                        // duplicate CompanyName
                        return View();
                    }
                    // Generate guid for this customer
                    employee.UserGuid = System.Guid.NewGuid();
                    // Hash & Salt the customer Password using SHA-1 algorithm
                    employee.Password = UserAccount.HashSHA1(employee.Password + employee.UserGuid);
                    // save customer to database
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }catch(Exception ex)
            {
                if (((DbEntityValidationException)ex).EntityValidationErrors != null)
                { 
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();

                    IEnumerable<DbEntityValidationResult> entityValidationExceptions =
                        ((DbEntityValidationException)ex).EntityValidationErrors;

                    foreach (DbEntityValidationResult exception in entityValidationExceptions)
                    {


                        foreach (DbValidationError error in exception.ValidationErrors)
                        {
                            sb.AppendLine(error.PropertyName + " : " + error.ErrorMessage);
                        }
                    }
                    sb.AppendLine(ex.Message + " " + ex.InnerException?.Message);
                }
            }

            return View();
        }
        // POST: Employee/Signin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn([Bind(Include = "EmployeeId,Password")] EmployeeSignIn employeeSignin, string ReturnUrl)

        {
            using (carNorthwindsEntities db = new carNorthwindsEntities())
            {
                // find customer by CustomerId
                Employee employee = db.Employees.Find(employeeSignin.EmployeeId);
                // hash & salt the posted password
                string str = UserAccount.HashSHA1(employeeSignin.Password + employee.UserGuid);
                // Compared posted Password to customer password
                if (str == employee.Password)
                {
                    // Passwords match
                    // authenticate user (this stores the CustomerID in an encrypted cookie)
                    // normally, you would require HTTPS
                    FormsAuthentication.SetAuthCookie(employee.EmployeeID.ToString(), false);
                    // send a cookie to the client to indicate that this is a customer
                    HttpCookie myCookie = new HttpCookie("role");
                    myCookie.Value = "employee";
                    Response.Cookies.Add(myCookie);
                    // if there is a return url, redirect to the url
                    if (ReturnUrl != null)
                    {
                        return Redirect(ReturnUrl);
                    }
                    // Redirect to Home page
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
                else
                {
                    // Passwords do not match
                }
                // create drop-down list box for company name
                ViewBag.CustomerID = new SelectList(db.Employees.OrderBy(c => c.Email), "Email", "Email").ToList();
                return View();
            }
        }
        [Authorize]
        [HttpPost]
        // POST: Employee/Account
        [ValidateAntiForgeryToken]
        public ActionResult Account([Bind(Include = "FirstName,LastName,Title,Address,City,Region,PostalCode,Country,HomePhone,Email")] EmployeeEdit UpdatedEmployee)
        {
            // For future version, make sure that an authenticated user is a customer
            if (Request.Cookies["role"].Value != "employee")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (carNorthwindsEntities db = new carNorthwindsEntities())
            {
                Employee employee = db.Employees.Find(UserAccount.GetUserID());
                //customer.CompanyName = UpdatedCustomer.CompanyName;
                // if the customer is changing their CompanyName
                if (employee.Email.ToLower() != UpdatedEmployee.Email.ToLower())
                {
                    // Ensure that the CompanyName is unique
                    if (db.Employees.Any(e => e.Email == UpdatedEmployee.Email))
                    {
                        // duplicate CompanyName
                        return View(UpdatedEmployee);
                    }
                    employee.Email = UpdatedEmployee.Email;
                }
                employee.Address = UpdatedEmployee.Address;
                employee.City = UpdatedEmployee.City;
                employee.FirstName = UpdatedEmployee.FirstName;
                employee.LastName = UpdatedEmployee.LastName;
                employee.Title = UpdatedEmployee.Title;
                employee.Country = UpdatedEmployee.Country;
                employee.Email = UpdatedEmployee.Email;
                employee.HomePhone = UpdatedEmployee.HomePhone;
                employee.PostalCode = UpdatedEmployee.PostalCode;
                employee.Region = UpdatedEmployee.Region;

                db.SaveChanges();
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }


 

        public ActionResult Charts()
        {
            using (carNorthwindsEntities db = new carNorthwindsEntities())
            {
                // create drop-down list box for company name
                ViewBag.FirstName = new SelectList(db.Employees.OrderBy(e => e.EmployeeID), "FirstName", "FirstName").ToList();
            }
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Charts([Bind(Include = "FirstName,StartDate, EndDate")] ChartData chartdata, string ReturnUrl)
        {     
            using(var db = new carNorthwindsEntities())
            {
                Employee emp = db.Employees.Where(e => e.FirstName == chartdata.FirstName).FirstOrDefault();
                var employeeID = emp.EmployeeID;

                var orders = (from E in db.Employees
                              orderby E.EmployeeID
                              select new
                              {
                                  E.EmployeeID,
                                  Orders = E.Orders.Count()
                              }).ToList();
                return Json(orders , JsonRequestBehavior.AllowGet);
            }
            
        }


    }
}
