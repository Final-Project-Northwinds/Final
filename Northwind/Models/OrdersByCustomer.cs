﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Northwind.Models
{
    public class OrdersByCustomer
    {
        public int NumOfOrders {get;set;}
        public String CompanyName { get; set; }
        public String EmployeeID { get; set; }

    }
}