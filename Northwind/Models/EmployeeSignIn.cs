﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Northwind.Models
{
    public class EmployeeSignIn
    {
        public int EmployeeId{ get; set; }
        public string Password { get; set; }
    }
}