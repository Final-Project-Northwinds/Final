﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Northwind.Models;

namespace Northwind.Models
{
    public class ChartData
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int OrderId { get; set; }
        public string FirstName { get; set; }

    }
}
